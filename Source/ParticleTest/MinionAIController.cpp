// Fill out your copyright notice in the Description page of Project Settings.


#include "MinionAIController.h"
#include "Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "Engine/World.h"

AMinionAIController::AMinionAIController()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AMinionAIController::BeginPlay()
{
	Super::BeginPlay();
}

void AMinionAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AMinionAIController::MoveToPlayer()
{
	
}

void AMinionAIController::Avoidance()
{
	isAvoiding = false;

	/*
	// Separation between minions
	for (int i = 0; i < bossController->MinionList.Num(); i++)
	{		
		if (bossController->MinionList[i] != minionActor)
		{
			if (bossController->MinionList.Num() > 0)
			{
				float distanceWithNeighbour = FVector::Distance(bossController->MinionList[i]->GetActorLocation(), minionActor->GetActorLocation());
				if (distanceWithNeighbour < boidRadius)
				{
					direction = minionActor->GetActorLocation() - bossController->MinionList[i]->GetActorLocation();
					direction.GetSafeNormal(1.0f);
					direction.Normalize(1.0f);
					if (!minionActor->inAtkRadius)
					{
						acceleration += (direction /
							distanceWithNeighbour * boidRadius * repelForce * decreaseFactor) * currentVelocity.Size();
					}
					else
					{
						acceleration += (direction /
							distanceWithNeighbour * boidRadius *(repelForce - 0.1f)* decreaseFactor);
					}
				}
			}
		}
	}

	for (int i = 0; i < bossController->FireMinionList.Num(); i++)
	{
		if (bossController->FireMinionList[i] != minionActor)
		{
			float distanceWithNeighbour = FVector::Distance(bossController->FireMinionList[i]->GetActorLocation(), minionActor->GetActorLocation());
			if (distanceWithNeighbour < boidRadius)
			{
				direction = minionActor->GetActorLocation() - bossController->FireMinionList[i]->GetActorLocation();
				direction.GetSafeNormal(1.0f);
				direction.Normalize(1.0f);
				if (!minionActor->inAtkRadius)
				{
					acceleration += (direction /
						distanceWithNeighbour * boidRadius * repelForce * decreaseFactor) * currentVelocity.Size();
				}
				else
				{
					acceleration += (direction /
						distanceWithNeighbour * boidRadius * (repelForce - 0.1f)* decreaseFactor);
				}
			}
		}
	}

	// Separation between boss
	float distanceToBoss = FVector::Distance(bossController->bossActor->GetActorLocation(), minionActor->GetActorLocation());
	if(distanceToBoss <= bossAvoidDistance)
	{
		direction = minionActor->GetActorLocation() - bossController->bossActor->GetActorLocation();
		direction.GetSafeNormal(1.0f);
		direction.Normalize(1.0f);
		if (!minionActor->inAtkRadius)
		{
			acceleration += (direction /
				distanceToBoss * bossAvoidDistance * repelForce * decreaseFactor) * currentVelocity.Size();
		}
		else
		{
			acceleration += (direction /
			distanceToBoss * bossAvoidDistance * (repelForce - 0.1f) * decreaseFactor);
		}
	}

	// Separation between Player	
	if (distanceToPlayer < playerAvoidDistance)
	{
		direction = minionActor->GetActorLocation() - playerCharacter->GetActorLocation();
		direction.GetSafeNormal(1.0f);
		direction.Normalize(1.0f);
		isAvoiding = true;
		if (!minionActor->inAtkRadius)
		{
			acceleration += (direction /
			distanceToPlayer * playerAvoidDistance * repelForce * decreaseFactor) * currentVelocity.Size();
		}
		else
		{
			acceleration += (direction  /
			distanceToPlayer * playerAvoidDistance * repelForce * decreaseFactor);
		}
	}
	*/
	// Separation between shootingLine
	//for (int i = 0; i < bossController->MinionList.Num(); i++)
	//{
	//	for (int j = 0; j < bossController->MinionList[i]->shootingLine.Num(); j++)
	//	{	
	//		if (bossController->MinionList[i]->shootingLine[j])
	//		{
	//			FVector point = bossController->MinionList[i]->shootingLine[j]->GetActorLocation();
	//			float distanceWithLine = FVector::Distance(point, minionActor->GetActorLocation());
	//			if (distanceWithLine < boidRadius)
	//			{
	//				direction = minionActor->GetActorLocation() - point;
	//				direction.GetSafeNormal(1.0f);
	//				direction.Normalize(1.0f);
	//				if (!minionActor->inAtkRadius)
	//				{
	//					acceleration += (direction /
	//						distanceWithLine * boidRadius * repelForce * decreaseFactor) * currentVelocity.Size();
	//				}
	//				else
	//				{
	//					acceleration += (direction /
	//						distanceWithLine * boidRadius *(repelForce - 0.1f)* decreaseFactor);
	//				}
	//			}
	//		}			
	//	}		
	//}
}

void AMinionAIController::Roaming()
{
	/*if (minionActor->enemyType == EnemyType::Normal)
	{
		MoveToLocation((-minionActor->GetActorForwardVector() * 200.0f) + minionActor->GetActorLocation(), 100.0f, false);
		isAvoiding = true;*/
		/*
		direction = -(minionActor->GetActorForwardVector());
		
		acceleration += direction * (accelerationForce * decreaseFactor);

		// Separation between boss
		float distanceToBoss = FVector::Distance(bossController->bossActor->GetActorLocation(), minionActor->GetActorLocation());
		if (distanceToBoss <= bossAvoidDistance)
		{
			direction = minionActor->GetActorLocation() - bossController->bossActor->GetActorLocation();
			direction.GetSafeNormal(1.0f);
			direction.Normalize(1.0f);

			acceleration += (direction /
				distanceToBoss * bossAvoidDistance * (repelForce - 0.1f) * decreaseFactor);
		}

		// Separtion between Player	
		if (distanceToPlayer < playerAvoidDistance)
		{
			direction = minionActor->GetActorLocation() - playerCharacter->GetActorLocation();
			direction.GetSafeNormal(1.0f);
			direction.Normalize(1.0f);
			if (!minionActor->inAtkRadius)
			{
				acceleration += (direction /
					distanceToPlayer * playerAvoidDistance * repelForce * decreaseFactor) * currentVelocity.Size();
			}
			else
			{
				acceleration += (direction /
					distanceToPlayer * playerAvoidDistance * repelForce * decreaseFactor);
			}
		}
		
	}*/
	
	//Avoidance();
}

void AMinionAIController::FindBossActor(UWorld* World, TArray<ABossCharacter*>& Out)
{
	for (TActorIterator<ABossCharacter> It(World); It; ++It)
	{
		Out.Add(*It);
	}
}




