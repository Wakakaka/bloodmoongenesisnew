// Fill out your copyright notice in the Description page of Project Settings.


#include "NormalMinion.h"
#include "NormalMinionAIController.h"

ANormalMinion::ANormalMinion()
{
	enemyType = EnemyType::Normal;
	maxHealth = 100.0f;
	currentHealth = maxHealth;
}

void ANormalMinion::BeginPlay()
{
	Super::BeginPlay();
	minionAI = Cast<ANormalMinionAIController>(GetController());
	atkResetDuration = FMath::RandRange(3.0f, 5.0f);
	SetAttackType();
}

void ANormalMinion::SetAttackType()
{
	int randNum = FMath::RandRange(0, 1);
	if (randNum == 0)
	{
		attackType = MinionAttackType::Claw;
	}
	else
	{
		attackType = MinionAttackType::Lunge;
	}
	followTimer = 0.0f;
}

// Called every frame
void ANormalMinion::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!isOutOfBloodPool)
	{
		return;
	}

	if (isAtk || isRoaming)
	{
		atkResetTimer += DeltaTime;
		if (atkResetTimer >= atkResetDuration)
		{
			atkResetTimer = 0.0f;
			atkResetDuration = FMath::RandRange(3.0f, 5.0f);
			isAtk = false;
			isRoaming = false;
		}
	}
	else if (!isAtk)
	{
		followTimer += DeltaTime;
		if (followTimer >= followDuration)
		{
			SetAttackType();
		}
	}

	if (isStunned)
	{
		stunTimer += DeltaTime;
		if (stunTimer >= stunDuration)
		{
			stunTimer = 0.0f;
			isStunned = false;
			ResetAnimation();
		}
	}

	if (isLookAtPlayer)
	{
		LookAtPlayer(10.0f);
	}
}

void ANormalMinion::Kill()
{
	if (minionAI->bossController)minionAI->bossController->MinionList.Remove(this);
	Destroy();
}

void ANormalMinion::Stun(FVector source)
{
	stunTimer = 0.0f;
	if (!isStunned) isStunned = true;
	minionAI->StopMovement();
	FVector sourceLocation = source;
	sourceLocation.Z = GetActorLocation().Z;
	FVector directionToSource = sourceLocation - GetActorLocation();
	FRotator Rot = FRotationMatrix::MakeFromX(directionToSource).Rotator();
	SetActorRotation(Rot);
}