// Fill out your copyright notice in the Description page of Project Settings.


#include "NormalMinionAIController.h"
#include "GameFramework/Character.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "Engine/World.h"

ANormalMinionAIController::ANormalMinionAIController()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ANormalMinionAIController::BeginPlay()
{
	Super::BeginPlay();
}

void ANormalMinionAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!minionActor)
	{
		minionPawn = GetPawn();
		minionActor = Cast<ANormalMinion>(minionPawn);
		playerCharacter = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	}

	if (minionActor->isStunned)
	{
		return;
	}

	if (minionActor->isInArena)
	{
		if (!isFoundBoss)
		{
			isFoundBoss = true;
			GetBossAIController();
		}

		if (!minionActor->isOutOfBloodPool)
		{
			return;
		}

		if (minionActor->playerActor->CurrentHealth <= 0.0f || bossCharacterList[0]->currentHealth <= 0.0f)
		{
			if (FVector::Distance(minionActor->GetActorLocation(), playerCharacter->GetActorLocation()) < safeRadius)
			{
				Roaming();
			}
			else
			{
				StopMovement();
			}
			return;
		}
	}
	else
	{
		if (!minionActor->isOutOfBloodPool)
		{
			return;
		}
		if (minionActor->playerActor->CurrentHealth <= 0.0f)
		{
			if (FVector::Distance(minionActor->GetActorLocation(), playerCharacter->GetActorLocation()) < safeRadius)
			{
				Roaming();
			}
			else
			{
				StopMovement();
			}
			return;
		}
	}

	
	if (minionActor->isRoar)
	{
		StopMovement();
		minionActor->roarExplodeTimer += DeltaTime;
		if (minionActor->roarExplodeTimer >= minionActor->roarExplodeDuration)
		{
			minionActor->RoarExplode();
		}
	}

	if (playerCharacter && minionActor)
	{
		distanceToPlayer = FVector::Distance(minionActor->GetActorLocation(), playerCharacter->GetActorLocation());
		if (minionActor->attackType == MinionAttackType::Claw)
		{
			if (distanceToPlayer <= 170.0f)
			{
				minionActor->inAtkRadius = true;
			}
			else
			{
				if (minionActor->inAtkRadius == true)  minionActor->inAtkRadius = false;
			}
		}
		else
		{
			if (distanceToPlayer <= 350.0f)
			{
				minionActor->inAtkRadius = true;
			}
			else
			{
				if (minionActor->inAtkRadius == true)  minionActor->inAtkRadius = false;
			}
		}

		if (!minionActor->isRoaming && !minionActor->isAtk && !minionActor->isStunned)
		{
			MoveToPlayer();
			angle = 0.0f;
			minionActor->LookAtPlayer(10.0f);
		}
		else if (minionActor->isRoaming && distanceToPlayer <= safeRadius && !minionActor->isStunned)
		{
			Roaming();
			minionActor->LookAtPlayer(10.0f);
			if (distanceToPlayer > safeRadius)
			{
				StopMovement();
			}
		}
		else
		{
			if(!minionActor->isStunned)
			minionActor->LookAtPlayer(10.0f);
		}
	}

	if (minionActor->inAtkRadius && !minionActor->isAtk && !minionActor->isStunned)
	{
		minionActor->Attack();
		minionActor->isAtk = true;
		StopMovement();
	}
}

void ANormalMinionAIController::MoveToPlayer()
{
	if (!minionActor->inAtkRadius)
	{
		MoveToLocation(FVector(playerCharacter->GetActorLocation().X, playerCharacter->GetActorLocation().Y, minionActor->GetActorLocation().Z), 100.0f, false);
	}
}

void ANormalMinionAIController::Roaming()
{
	MoveToLocation((-minionActor->GetActorForwardVector() * 200.0f) + minionActor->GetActorLocation(), 100.0f, false);
}

void ANormalMinionAIController::GetBossAIController()
{
	FindBossActor(GetWorld(), bossCharacterList);
	bossController = Cast<AMyAIController>(bossCharacterList[0]->GetController());
}

