// Fill out your copyright notice in the Description page of Project Settings.


#include "BruteMinionAIController.h"
#include "GameFramework/Character.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "Engine/World.h"

ABruteMinionAIController::ABruteMinionAIController()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ABruteMinionAIController::BeginPlay()
{
	Super::BeginPlay();
}

void ABruteMinionAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!minionActor)
	{
		minionPawn = GetPawn();
		minionActor = Cast<ABruteMinion>(minionPawn);
		playerCharacter = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	}
	else
	{
		if (minionActor->isStunned)
		{
			return;
		}

		if (minionActor->isInArena)
		{
			if (!isFoundBoss)
			{
				isFoundBoss = true;
				GetBossAIController();
			}

			if (minionActor->playerActor)
			{
				if (minionActor->playerActor->CurrentHealth <= 0.0f || bossCharacterList[0]->currentHealth <= 0.0f)
				{
					StopMovement();
					return;
				}
			}
		}
		else
		{
			if (minionActor->playerActor->CurrentHealth <= 0.0f)
			{
				StopMovement();
				return;
			}
		}


		if (minionActor->isRoar)
		{
			StopMovement();
			minionActor->roarExplodeTimer += DeltaTime;
			if (minionActor->roarExplodeTimer >= minionActor->roarExplodeDuration)
			{
				minionActor->RoarExplode();
			}
		}

		if (playerCharacter && minionActor && minionActor->bruteMinionState != BruteMinionState::Attack &&
			minionActor->bruteMinionState != BruteMinionState::Vulnerable)
		{
			distanceToPlayer = FVector::Distance(minionActor->GetActorLocation(), playerCharacter->GetActorLocation());
			minionActor->LookAtPlayer(10.0f);
			if (distanceToPlayer <= 200.0f)
			{
				minionActor->inAtkRadius = true;
			}
			else
			{
				if (minionActor->inAtkRadius == true) minionActor->inAtkRadius = false;
			}

			if (!minionActor->isAtk)
			{
				if (minionActor->inAtkRadius)
				{
					StopMovement();
					idleTimer += DeltaTime;
					if (idleTimer >= idleDuration)
					{
						idleTimer = 0.0f;
						minionActor->bruteMinionState = BruteMinionState::Attack;
						minionActor->isAtk = true;
						minionActor->CheckSwing();
						minionActor->PlaySwingAttack();
					}
				}
				else
				{
					MoveToPlayer();
				}
			}
		}
	}
}

void ABruteMinionAIController::MoveToPlayer()
{
	if (!minionActor->inAtkRadius)
	{
		MoveToLocation(FVector(playerCharacter->GetActorLocation().X, playerCharacter->GetActorLocation().Y, minionActor->GetActorLocation().Z), 100.0f, false);
	}
}

void ABruteMinionAIController::ResetIdleTimer()
{
	idleTimer = 0.0f;
}

void ABruteMinionAIController::SetIdleDuration(float t)
{
	idleDuration = t;
}

void ABruteMinionAIController::SetIdleTimerReached()
{
	idleTimer = idleDuration;
}

void ABruteMinionAIController::GetBossAIController()
{
	FindBossActor(GetWorld(), bossCharacterList);
	bossController = Cast<AMyAIController>(bossCharacterList[0]->GetController());
}
