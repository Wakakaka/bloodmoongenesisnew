// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine.h"
#include "Engine/World.h"
#include "Enemy.generated.h"

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EPushbackTypeEnum : uint8
{

	EP_Short 	UMETA(DisplayName = "Short"),
	EP_Medium	UMETA(DisplayName = "Medium"),
	EP_Long	    UMETA(DisplayName = "Long"),
	EP_ArkThrowSlide UMETA(DisplayName = "ArkThrow_Slide"),
	EP_ArkThrowRollback UMETA(DisplayName = "ArkThrow_Rollback")
};

UCLASS()
class PARTICLETEST_API AEnemy : public ACharacter
{
	GENERATED_BODY()



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	bool isDamaged = false;
	

public:
	
	// Sets default values for this character's properties
	AEnemy();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	float maxHealth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	float currentHealth;

	UPROPERTY(BlueprintReadWrite, Category = "Lock On Properties")
		float LockonThresholdDistance;
	UPROPERTY(BlueprintReadWrite, Category = "Lock On Properties")
		UChildActorComponent* lockOnPointerLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attack")
		bool isAtk = false;

	bool inAtkRadius = false;
		
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	void RemoveLockOn();
	UPROPERTY(BlueprintReadWrite, Category = "Lock On Properties")
		bool isTargetedByPlayer;

	UPROPERTY(BlueprintReadWrite, Category = "Lock On Properties")
		bool CanBeTargeted;
	UPROPERTY(BlueprintReadWrite)
		float MaximumDistance;
	UPROPERTY(BlueprintReadWrite)
		float ClosestTargetToMeDistance;

	UPROPERTY(BlueprintReadWrite, Category = "Is Damaged")
	bool isGetDamaged = false;

	UPROPERTY(BlueprintReadWrite, Category = "Lock On Properties")
		AEnemy* ClosestTargetToMe;
	UPROPERTY(BlueprintReadWrite, Category = "Lock On Properties")
		float PlayerToEnemyDistance;
	

	UFUNCTION(BlueprintCallable, Category = "Damage")
		void SetIsDamaged(bool b);

	UFUNCTION(BlueprintCallable, Category = "Damage")
		bool GetIsDamaged();

	UPROPERTY(BlueprintReadWrite, Category = "Damage")
		UChildActorComponent* hitParticleSocketLocation;
	
	ACharacter* playerPawn;
	class ATPSCharacter* playerActor;

	UFUNCTION(BlueprintCallable, Category = "Look At Player")
		void LookAtPlayer(float speed);
};
