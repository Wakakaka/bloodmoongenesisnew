#include "BruteMinion.h"
#include "BruteMinionAIController.h"

ABruteMinion::ABruteMinion()
{
	enemyType = EnemyType::Brute;
	maxHealth = 300.0f;
	currentHealth = maxHealth;
	damage = 12.0f;
	stunDuration = 1.7f;
}

void ABruteMinion::BeginPlay()
{
	Super::BeginPlay();
	minionAI = Cast<ABruteMinionAIController>(GetController());
	SetMinionState();
	ResetStanceRecovery();
}

void ABruteMinion::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (isStunned)
	{
		stunTimer += DeltaTime;
		if (stunTimer >= stunDuration)
		{
			stunTimer = 0.0f;
			isStunned = false;
			ResetAttack();
		}
	}

	/*if (bruteMinionState == BruteMinionState::Vulnerable)
	{
		vulnerableResetTimer += DeltaTime;
		if (vulnerableResetTimer >= vulnerableResetDuration)
		{
			vulnerableResetTimer = 0.0f;
			ResetVulnerable();
		}
	}*/

	if (!isAtk)
	{
		if (currentStance > 0.0f)
		{
			if (stanceRecoveryTimer >= stanceRecoveryDuration)
			{
				currentStance -= DeltaTime * stanceRecoverySpeed;
				if (currentStance <= 0.0f)
				{
					currentStance = 0.0f;
					stanceRecoveryTimer = 0.0f;
				}
			}
			else
			{
				stanceRecoveryTimer += DeltaTime;
			}
		}
	}

	if (isLookAtPlayer)
	{
		LookAtPlayer(10.0f);
	}
}

void ABruteMinion::Stun(FVector source)
{
	stunTimer = 0.0f;
	if (!isStunned) isStunned = true;
	minionAI->StopMovement();
	FVector sourceLocation = source;
	sourceLocation.Z = GetActorLocation().Z;
	FVector directionToSource = sourceLocation - GetActorLocation();
	FRotator Rot = FRotationMatrix::MakeFromX(directionToSource).Rotator();
	SetActorRotation(Rot);
}

void ABruteMinion::Kill()
{
	RemoveLockOn();
	if(minionAI->bossController)minionAI->bossController->MinionList.Remove(this);
	Destroy();	
}

void ABruteMinion::AddStance(float s)
{
	if (bruteMinionState != BruteMinionState::Vulnerable)
	{
		currentStance += s;
		if (currentStance >= maxStance)
		{
			SetVulnerable();
			currentStance = 0.0f;
		}
		ResetStanceRecovery();
	}
}

void ABruteMinion::ResetStanceRecovery()
{
	stanceRecoveryTimer = 0.0f;
	stanceRecoveryDuration = (maxHealth / currentHealth) * 1.5f;
	stanceRecoverySpeed = (currentHealth / maxHealth) * 20.0f;
}

void ABruteMinion::SetMinionState()
{
	int randNum = FMath::RandRange(1, 100);
	if (randNum <= 40)
	{
		bruteMinionState = BruteMinionState::BlockWalk;
		minionAI->SetIdleDuration(FMath::RandRange(blockWalkAttackMinTimer, blockWalkAttackMaxTimer));
	}
	else
	{
		bruteMinionState = BruteMinionState::Walk;
		minionAI->SetIdleDuration(FMath::RandRange(walkAttackMinTimer, walkAttackMaxTimer));
	}
	SetWalkAnimation();
}

void ABruteMinion::SetVulnerable()
{	
	if (bruteMinionState != BruteMinionState::Attack)
	{
		isInterupted = true;
	}
	isAtk = false;
	bruteMinionState = BruteMinionState::Vulnerable;
	minionAI->StopMovement();
	PlayVulnerable();
}

void ABruteMinion::ResetVulnerable()
{
	SetMinionState();
	ResetAnimation();
	if (isInterupted)
	{
		minionAI->SetIdleTimerReached();
		isInterupted = false;
	}
}

void ABruteMinion::ResetAttack()
{
	isAtk = false;
	SetMinionState();
	minionAI->ResetIdleTimer();
	ResetAnimation();
}

void ABruteMinion::CheckSwing()
{
	int randNum = FMath::RandRange(1, 100);
	if (randNum <= 50)
	{
		bruteAttackType = BruteMinionAttackType::OneSwing;
	}
	else
	{
		bruteAttackType = BruteMinionAttackType::TwoSwing;
	}
}

void ABruteMinion::CheckSlam()
{
	int randNum = FMath::RandRange(1, 100);
	if (randNum <= 50)
	{
		PlaySlamAttack();
	}
	else
	{
		ResetAttack();
	}
}

bool ABruteMinion::CheckPercentage(int p)
{
	int randNum = FMath::RandRange(1, 100);

	if (randNum <= p)
	{
		return true;
	}
	else
	{
		return false;
	}
}
