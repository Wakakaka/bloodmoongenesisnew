// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MinionAIController.h"
#include "BruteMinion.h"
#include "BruteMinionAIController.generated.h"

UCLASS()
class PARTICLETEST_API ABruteMinionAIController : public AMinionAIController
{
	GENERATED_BODY()
private:
	class ABruteMinion* minionActor;
	float idleTimer = 0.0f;
	float idleDuration = 2.0f;

public:
	ABruteMinionAIController();

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;
	virtual void MoveToPlayer() override;
	void SetIdleDuration(float t);
	void ResetIdleTimer();
	void SetIdleTimerReached();

	UFUNCTION(BlueprintCallable, Category = "Minion Behavior")
	void GetBossAIController();
};
