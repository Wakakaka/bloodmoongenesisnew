// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Minion.h"
#include "BloodProjectile.h"
#include "OniHeadMinion.generated.h"

UCLASS()
class PARTICLETEST_API AOniHeadMinion : public AMinion
{
	GENERATED_BODY()
	
protected:
	virtual void BeginPlay() override;

private:
	class AOniHeadMinionAIController* minionAI;

public:
	AOniHeadMinion();

	virtual void Tick(float DeltaTime) override;
	
	virtual void Kill() override;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Explode")
	void Explode();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Animation")
	void PlayExplode();

	void SetMaxSpeed(float speed);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Minion Behavior")
	bool isExploded = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Minion Behavior")
		USceneComponent* skeletalMesh;

	virtual void Stun(FVector source) override;
};
