// Fill out your copyright notice in the Description page of Project Settings.


#include "OniHeadMinionAIController.h"
#include "OniHeadMinion.h"
#include "Engine/World.h"

AOniHeadMinionAIController::AOniHeadMinionAIController()
{

}

void AOniHeadMinionAIController::BeginPlay()
{
	Super::BeginPlay();
	FindBossActor(GetWorld(), bossCharacterList);
	bossController = Cast<AMyAIController>(bossCharacterList[0]->GetController());
}

void AOniHeadMinionAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!minionActor)
	{
		minionPawn = GetPawn();
		minionActor = Cast<AOniHeadMinion>(minionPawn);
		//playerCharacter = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	}

	if (minionActor->isRoar)
	{
		StopMovement();
		minionActor->roarExplodeTimer += DeltaTime;
		if (minionActor->roarExplodeTimer >= minionActor->roarExplodeDuration)
		{
			minionActor->RoarExplode();
		}
	}
	else if (minionActor->playerActor && minionActor)
	{
		distanceToPlayer = FVector::Distance(minionActor->GetActorLocation(), minionActor->playerActor->GetActorLocation());
		minionActor->LookAtPlayer(10.0f);
		Floating(DeltaTime);
		if (!minionActor->isExploded || !minionActor->isStunned)
		{
			if(distanceToPlayer > 200.0f) MoveToPlayer();
			else if (distanceToPlayer <= 180.0f)
			{
				Roaming();
			}
			else if (distanceToPlayer > 180.0f && distanceToPlayer <= 200.0f)
			{
				StopMovement();
			}
		}
		if(!minionActor->isAtk)
		{			
			if (distanceToPlayer <= 200.0f)
			{
				minionActor->isAtk = true;
				minionActor->SetMaxSpeed(300.0f);
				minionActor->PlayExplode();
			}
		}
		else
		{
			Vibrating(DeltaTime);
			if (!minionActor->isExploded)
			{				
				if (explodeTimer >= explodeDuration)
				{
					StopMovement();
					minionActor->isExploded = true;
					minionActor->Explode();
				}
				else
				{
					explodeTimer += DeltaTime;
				}
			}			
		}
	}
}

void AOniHeadMinionAIController::MoveToPlayer()
{
	if (!minionActor->inAtkRadius)
	{
		MoveToLocation(FVector(minionActor->playerActor->GetActorLocation().X, minionActor->playerActor->GetActorLocation().Y,
			minionActor->GetActorLocation().Z), 100.0f, false);
	}
}

void AOniHeadMinionAIController::Roaming()
{
	MoveToLocation(minionActor->GetActorLocation() + (-minionActor->GetActorForwardVector()*150.0f), 100.0f, false);
}

void AOniHeadMinionAIController::Floating(float DeltaTime)
{
	angle += DeltaTime * randSpeed;
	float rad = angle * (PI / 180.0f);
	FVector iniLoc = minionActor->GetMesh()->GetComponentLocation();
	iniLoc.Z = iniLoc.Z + cosf(rad);
	minionActor->GetMesh()->SetWorldLocation(iniLoc);

	if (angle >= 360.0f)
	{
		angle = -360.0f;
	}
}

void AOniHeadMinionAIController::Vibrating(float DeltaTime)
{
	vibrateTimer += DeltaTime;
	randomScaleTimer += DeltaTime;
	randomScaleDuration = 0.13f / vibrateTimer;
	if (randomScaleTimer >= randomScaleDuration)
	{
		randomScaleTimer = 0.0f;
		float minRand = 0.9f - vibrateTimer * 0.2f;
		float maxRand = 1.1f + vibrateTimer * 0.2f;
		scale.X = FMath::RandRange(minRand, maxRand);
		scale.Y = FMath::RandRange(minRand, maxRand);
		scale.Z = FMath::RandRange(minRand, maxRand);
		alpha = 0.0f;
	}
	alpha += (DeltaTime * (vibrateTimer *0.8f));
	if (alpha > 1.0f)
	{
		alpha = 1.0f;
	}

	minionActor->GetMesh()->SetWorldScale3D(FMath::Lerp(minionActor->GetMesh()->GetComponentScale(), scale, alpha));
}

