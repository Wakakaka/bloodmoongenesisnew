// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"
#include "TPSCharacter.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!playerPawn)
	{
		playerPawn = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
		playerActor = Cast<ATPSCharacter>(playerPawn);
	}
}

void AEnemy::RemoveLockOn()
{
	
	
	if (playerActor->LockedTarget == this && playerActor->TargetLocked && isTargetedByPlayer)
	{
		FOutputDeviceNull ar;
		playerActor->CallFunctionByNameWithArguments(TEXT("FindCloseEnemyToTargetLockedAfterDeath"), ar, NULL, true);
	}
	
		
		
	
	
}




// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemy::SetIsDamaged(bool b)
{
	isDamaged = b;
}

bool AEnemy::GetIsDamaged()
{
	return isDamaged;
}

void AEnemy::LookAtPlayer(float speed)
{
	if (playerPawn)
	{
		FVector playerPos = playerPawn->GetActorLocation();
		FVector bossPos = GetActorLocation();
		playerPos.Z = 0.0f;
		bossPos.Z = 0.0f;
		FRotator Rot = FRotationMatrix::MakeFromX(playerPos - bossPos).Rotator();
		if (speed == -1.0f) SetActorRotation(Rot);
		else SetActorRotation(UKismetMathLibrary::RLerp(GetActorRotation(), Rot, GetWorld()->DeltaTimeSeconds * speed, true));
	}	
}

