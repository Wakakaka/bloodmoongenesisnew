// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MinionAIController.h"
#include "OniHeadMinionAIController.generated.h"

UCLASS()
class PARTICLETEST_API AOniHeadMinionAIController : public AMinionAIController
{
	GENERATED_BODY()
	
private:
	class AOniHeadMinion* minionActor;
	
	float explodeDuration = 5.0f;
	virtual void MoveToPlayer() override;
	void Floating(float DeltaTime);
	void Vibrating(float DeltaTime);
	float vibrateTimer = 1.0f;
	float randomScaleDuration = 0.0f;
	float randomScaleTimer = 0.0f;
	float alpha = 0.0f;
	FVector scale = FVector(1.0f,1.0f,1.0f);
	float angle = -360.0f;
	float randHeight = FMath::RandRange(1.0f, 1.5f);
	float randSpeed = FMath::RandRange(190.0f, 280.0f);

public:
	AOniHeadMinionAIController();

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	virtual void Roaming() override;

	UPROPERTY(EditAnywhere, Category = "Minion Behavior")
	float explodeTimer = 0.0f;
};
