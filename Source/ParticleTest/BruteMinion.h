// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Minion.h"
#include "BruteMinion.generated.h"

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class BruteMinionAttackType : uint8
{	
	OneSwing UMETA(DisplayName = "OneSwing"),
	TwoSwing UMETA(DisplayName = "TwoSwing"),
	ClubSlam UMETA(DisplayName = "ClubSlam")
};

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class BruteMinionState : uint8
{
	Walk UMETA(DisplayName = "Walk"),
	BlockWalk UMETA(DisplayName = "BlockWalk"),
	Vulnerable UMETA(DisplayName = "Vulnerable"),
	Attack UMETA(DisplayName = "Attack")
};

UCLASS()
class PARTICLETEST_API ABruteMinion : public AMinion
{
	GENERATED_BODY()
	
private:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	class ABruteMinionAIController* minionAI;
	float vulnerableResetTimer = 0.0f;
	float vulnerableResetDuration = 3.0f;
	void ResetStanceRecovery();
	float stanceRecoveryTimer = 0.0f;
	float stanceRecoveryDuration = 0.0f;
	float stanceRecoverySpeed = 0.0f;
	bool isInterupted = false;

public:

	

	ABruteMinion();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attack")
	BruteMinionAttackType bruteAttackType;

	virtual void Stun(FVector source) override;

	virtual void Kill() override;

	UFUNCTION(BlueprintImplementableEvent, Category = "Animation")
		void PlaySwingAttack();

	UFUNCTION(BlueprintImplementableEvent, Category = "Animation")
		void PlaySlamAttack();

	UFUNCTION(BlueprintImplementableEvent, Category = "Animation")
		void PlayVulnerable();

	UFUNCTION(BlueprintCallable, Category = "Stance")
	void SetVulnerable();

	UFUNCTION(BlueprintCallable, Category = "Stance")
	void ResetVulnerable();

	UFUNCTION(BlueprintCallable, Category = "Stance")
	void AddStance(float s);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stance")
		float currentStance = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stance")
		float maxStance = 100.0f;

	UFUNCTION(BlueprintCallable, Category = "Attack")
	void ResetAttack();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MinionState")
	BruteMinionState bruteMinionState;

	UFUNCTION(BlueprintImplementableEvent, Category = "MinionState")
	void SetWalkAnimation();

	UFUNCTION(BlueprintCallable, Category = "MinionState")
	void SetMinionState();

	void CheckSwing();

	UFUNCTION(BlueprintCallable, Category = "Attack")
	void CheckSlam();

	UFUNCTION(BlueprintCallable, Category = "CheckAction")
		bool CheckPercentage(int p);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AttackTimer")
		float walkAttackMinTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AttackTimer")
		float walkAttackMaxTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AttackTimer")
		float blockWalkAttackMinTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AttackTimer")
		float blockWalkAttackMaxTimer;
};
